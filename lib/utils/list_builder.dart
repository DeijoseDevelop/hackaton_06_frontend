import 'package:flutter/material.dart';
import 'package:frontend/entities/entities.dart';
import 'package:frontend/screens/screens.dart';
import 'package:frontend/widgets/widgets.dart';

class ListBuilder{
  static ListView showLista(List<Events>? data, BuildContext context){
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: data!.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (_) => BetweenSplashScreen(
                screen: DetailEventScreen(
                  url: data[index].url,
                  description: data[index].description,
                ),
              )
            ));
          },
          child: CardCustom(
            url: data[index].url,
            description: data[index].description,
          ),
        );
      },
    );
  }
}