

import 'package:flutter/material.dart';
import 'package:frontend/entities/entities.dart';
import 'package:frontend/utils/list_builder.dart';

class DataProvider {
  DataProvider();

  static Widget futureBuilder(
    BuildContext context, Future<List<Events>> registros
  ){
    return FutureBuilder<List<Events>>(
      future: registros,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListBuilder.showLista(snapshot.data, context);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        return const CircularProgressIndicator();
      },
    );
  }
}
