import 'package:flutter/material.dart';
import 'package:frontend/widgets/widgets.dart';

// ignore: must_be_immutable
class DetailEventScreen extends StatelessWidget {

  String url;
  String description;

  DetailEventScreen({
    Key? key,
    required this.url,
    required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/images/logo.png',
          fit: BoxFit.cover,
        ),
      ),
      body: Center(
        child: CardDetail(
          url: url,
          description: description,
        ),
      ),
    );
  }
}
