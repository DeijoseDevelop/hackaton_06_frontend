import 'package:flutter/material.dart';
import 'package:frontend/screens/between_splash_screen.dart';
import 'package:frontend/screens/screens.dart';
import 'package:frontend/services/services.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/images/logo.png',
          fit: BoxFit.cover,
        ),
      ),
      body: Center(
          child: SafeArea(
        child: Column(
          children: <Widget>[
            const SizedBox(height: 80),
            const _TitleHome(),
            const SizedBox(height: 100),
            const _WelcomeImage(),
            const SizedBox(height: 100),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const BetweenSplashScreen(
                    screen: EventsScreen()
                  ),
                ));
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Color.fromRGBO(4, 79, 141, 1)),
                foregroundColor: MaterialStateProperty.all(Colors.white),
                elevation: MaterialStateProperty.all(0),
              ),
              child: const Text(
                'Get Started',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              )
            ),
          ]
        ),
      )),
    );
  }
}

class _TitleHome extends StatelessWidget {
  const _TitleHome({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return Text(
      textAlign: TextAlign.center,
        'welcome to the chamber of commerce',
        style: const TextStyle(
          color: Color.fromRGBO(4, 79, 141, 1),
          fontSize: 25,
          fontWeight: FontWeight.bold,
        ),
    );
  }
}

class _WelcomeImage extends StatelessWidget {
  const _WelcomeImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 200,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            'assets/images/svg.png',
          ),
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}
