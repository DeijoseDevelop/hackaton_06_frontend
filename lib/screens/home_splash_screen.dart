import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:frontend/screens/screens.dart';
import 'package:lottie/lottie.dart';
import 'package:page_transition/page_transition.dart';

class HomeSplashScreen extends StatelessWidget {
  const HomeSplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      nextScreen: const HomeScreen(),
      backgroundColor: Color.fromARGB(255, 240, 240, 240),
      splash: Column(
        children: <Widget>[
          SizedBox(
            child: Lottie.asset('assets/government.json'),
          ),
          const Text(
            'Chambers of Commerce',
            style: TextStyle(
              color: Color.fromRGBO(4, 79, 141, 1),
              fontSize: 30,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
      splashIconSize: 500,
      duration: 2000,
      splashTransition: SplashTransition.slideTransition,
      pageTransitionType: PageTransitionType.topToBottom,
      animationDuration: const Duration(seconds: 1),
    );
  }
}
