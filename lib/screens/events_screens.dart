import 'package:flutter/material.dart';
import 'package:frontend/screens/screens.dart';
import 'package:frontend/services/services.dart';
import 'package:frontend/utils/data_builder.dart';

class EventsScreen extends StatelessWidget {
  const EventsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    EventService eventService = EventService();

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/images/logo.png',
          fit: BoxFit.cover,
        ),
      ),
      body: Center(
        child: DataProvider.futureBuilder(context, eventService.getEvents()),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => BetweenSplashScreen(
              screen: FormScreen()
            ),
          ));
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
