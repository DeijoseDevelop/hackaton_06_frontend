import 'package:flutter/material.dart';
import 'package:frontend/entities/entities.dart';
import 'package:frontend/screens/screens.dart';
import 'package:frontend/services/services.dart';
import 'package:frontend/ui/input_decorations.dart';

class FormScreen extends StatelessWidget {
  final TextEditingController _url = TextEditingController();
  final TextEditingController _description = TextEditingController();

  FormScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/images/logo.png',
          fit: BoxFit.cover,
        ),
      ),
      body: Center(
        child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            height: MediaQuery.of(context).size.height * 0.8,
            child: _PostForm(url: _url, description: _description)),
      ),
    );
  }
}

class _PostForm extends StatelessWidget {
  const _PostForm({
    Key? key,
    required TextEditingController url,
    required TextEditingController description,
  })  : _url = url,
        _description = description,
        super(key: key);

  final TextEditingController _url;
  final TextEditingController _description;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        //key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: 200,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/form.png'),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              TextFormField(
                autocorrect: false,
                decoration: InputDecorations.authInputDecoration(
                    labelText: 'URL', hintText: 'Enter the URL'),
                controller: _url,
              ),
              TextFormField(
                autocorrect: false,
                decoration: InputDecorations.authInputDecoration(
                    labelText: 'Description',
                    hintText: 'Enter the description'),
                controller: _description,
              ),
              const SizedBox(height: 50),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                      const Color.fromRGBO(55, 58, 140, 1)),
                ),
                onPressed: () {
                  EventService eventService = EventService();

                  eventService.createEvent(
                    image: _url.text,
                    description: _description.text,
                  );
                  Future.delayed(const Duration(seconds: 2), () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const BetweenSplashScreen(
                        screen: EventsScreen()
                      ),
                    ));
                  });
                },
                child: const Text('Submit'),
              ),
            ]),
      ),
    );
  }
}
