import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:frontend/screens/screens.dart';
import 'package:lottie/lottie.dart';
import 'package:page_transition/page_transition.dart';

class BetweenSplashScreen extends StatelessWidget {

  final screen;

  const BetweenSplashScreen({Key? key, required this.screen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      nextScreen: screen,
      splash: SizedBox(
        child: Lottie.asset('assets/loading.json'),
      ),
      splashIconSize: 400,
      duration: 1000,
      splashTransition: SplashTransition.fadeTransition,
      pageTransitionType: PageTransitionType.fade,
      animationDuration: const Duration(seconds: 1),
    );
  }
}
