import 'package:frontend/entities/entities.dart';
import 'dart:convert';
//import 'dart:io';
import 'package:http/http.dart' as http;

class EventService {
  EventService();

  final String _baseUrl = 'https://api-django-cc.herokuapp.com/api/events/';

  Future<List<Events>> getEvents() async {
    final url = Uri.parse(_baseUrl);
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final body = const Utf8Decoder().convert(response.bodyBytes);
      return eventsFromJson(body);
    }
    throw Exception('Failed to load events');
  }

  Future createEvent({required String image, required String description}) async {
    Map<String, dynamic> body = {
      'url': image,
      'description': description,
    };

    final url = Uri.parse(_baseUrl);
    final response = await http.post(url, body: body);
    if (response.statusCode == 201) {
      return response.body;
    }
    //throw Exception('Failed to load events');
  }
}
