import 'package:flutter/material.dart';
import 'package:frontend/screens/home_splash_screen.dart';
import 'package:frontend/screens/screens.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Hackaton 06',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        appBarTheme: const AppBarTheme(
          color: Color.fromRGBO(55, 58, 140, 1),
          centerTitle: true,
        ),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: Color.fromRGBO(55, 58, 140, 1),
        ),
      ),
      initialRoute: '/',
      routes: {
        '/': (_) => const HomeSplashScreen(),
        '/list_events': (_) => const EventsScreen(),
        '/form_screen': (_) => FormScreen(),
      },
      //home: const HomeScreen(),
    );
  }
}
