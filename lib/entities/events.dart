import 'dart:convert';

List<Events> eventsFromJson(String str) =>
    List<Events>.from(json.decode(str).map((x) => Events.fromJson(x)));

String eventsToJson(List<Events> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Events {
  Events({
    required this.url,
    required this.description,
  });

  String url;
  String description;

  Events copyWith({
    String? url,
    String? description,
  }) =>
      Events(
        url: url ?? this.url,
        description: description ?? this.description,
      );

  factory Events.fromJson(Map<String, dynamic> json) => Events(
        url: json["url"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
        "description": description,
      };

  String toJson2() => json.encode(toMap());

  Map<String, dynamic> toMap(){
    return {
      "url": url,
      "description": description,
    };
  }
}
